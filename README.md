# Discord Revamp
A revamped discord based on discord's current website.

# Downloads
- Powercord: `git clone https://gitdab.com/12/Discord-Revamp`
- Better Discord: `put "DiscordRevamp.theme.css" in your themes folder`

# Previews
<img src="https://roblox.is-terrible.com/MeD32H.png"/>
<img src="https://roblox.is-terrible.com/jdN2Vq.png"/>
<img src="https://roblox.is-terrible.com/o9t2mG.png"/>
